<div class="table-responsive-sm">
    <table class="table table-striped" id="proposals-table">
        <thead>
            <tr>    
                <th>User</th>
                <th>Company</th>
                <th>Code</th>
                <th>Title</th>
                <th>Period</th>
                <th>Due</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($proposals as $proposal)
            <tr>
            <td>{{ $proposal->user->name }}</td>
            <td>{{ $proposal->company->name }}</td>
            <td>{{ $proposal->code }}</td>
            <td>{{ $proposal->title }}</td>
            <td>{{ $proposal->period }} Bulan</td>
            <td>{{ Carbon\Carbon::parse($proposal->due)->format('d M Y') }}</td>
            <td>{{ $proposal->status == 1 ? "Review" : "Approved" }}</td>
                <td>
                    {!! Form::open(['route' => ['proposals.destroy', $proposal->id], 'method' => 'delete']) !!}

                    <div class='btn-group'>
                        <a href="{{ route('proposals.show', [$proposal->id]) }}" class='btn btn-success'>Tampil</a>
                        <a href="{{ route('proposals.edit', [$proposal->id]) }}" class='btn btn-info'>Edit</a>
                        {!! Form::button('Hapus', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>