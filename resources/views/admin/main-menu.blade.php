<!-- BEGIN: Main Menu-->

    <div class="main-menu material-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="user-profile">
            <div class="user-info text-center pb-2"><img class="user-img img-fluid rounded-circle w-25 mt-2" src="{{asset('admin/images/portrait/small/avatar-s-1.png')}}" alt="" />
                <!-- <div class="name-wrapper d-block dropdown mt-1"><a class="white dropdown-toggle ml-2" id="user-account" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-name">{{Auth::user()->name}}</span></a> -->
                    <!-- <div class="text-light">UX Designer</div> -->
                    <!-- <div class="dropdown-menu arrow"><a class="dropdown-item"><i class="material-icons align-middle mr-1">person</i><span class="align-middle">Profile</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">message</i><span class="align-middle">Messages</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">attach_money</i><span class="align-middle">Balance</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">settings</i><span class="align-middle">Settings</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">power_settings_new</i><span class="align-middle">Log Out</span></a></div> -->
                <!-- </div> -->
            </div>
        </div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="active"><a href="{{url('dashboard')}}"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
                </li>
                </li>
                <li class=" navigation-header"><span data-i18n="User Interface">Investasi</span><i class="material-icons nav-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="User Interface">more_horiz</i>
                </li>
                <li class=" nav-item"><a href="{{url('business-listings')}}"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="Order">Peluang Investasi</span></a>
                </li>
                <li class=" navigation-header"><span data-i18n="Admin">Admin</span><i class="material-icons nav-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="Ecommerce">more_horiz</i>
                </li>
                <li class=" nav-item"><a href="{{url('categories')}}"><i class="material-icons">add_shopping_cart</i><span class="menu-title" data-i18n="Category">Kategori</span></a>
                </li>
                <li class=" nav-item"><a href="{{url('companies')}}"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="Perusahaan">Perusahaan</span></a>
                </li>
                <li class=" nav-item"><a href="{{url('proposals')}}"><i class="material-icons">card_travel</i><span class="menu-title" data-i18n="Proposal">Proposal</span></a>
                </li>
                <li class=" nav-item"><a href="{{url('bills')}}"><i class="material-icons">local_atm</i><span class="menu-title" data-i18n="Tagihan">Tagihan</span></a>
                </li>
                <li class=" nav-item"><a href="{{url('users')}}"><i class="material-icons">done</i><span class="menu-title" data-i18n="Order">User</span></a>
                </li>
                <!-- <li class=" nav-item"><a href="#"><i class="material-icons">content_paste</i><span class="menu-title" data-i18n="Invoice">Invoice</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="invoice-summary.html"><i class="material-icons"></i><span data-i18n="Invoice Summary">Invoice Summary</span></a>
                        </li>
                        <li><a class="menu-item" href="invoice-template.html"><i class="material-icons"></i><span data-i18n="Invoice Template">Invoice Template</span></a>
                        </li>
                        <li><a class="menu-item" href="invoice-list.html"><i class="material-icons"></i><span data-i18n="Invoice List">Invoice List</span></a>
                        </li>
                    </ul>
                </li> -->
                <li class=" navigation-header"><span data-i18n="User Interface">User Interface</span><i class="material-icons nav-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="User Interface">more_horiz</i>
                </li>
                <li class=" nav-item"><a href="{{url('user-profile')}}"><i class="material-icons">done</i><span class="menu-title" data-i18n="Order">Profile</span></a>
                </li>
                <li class=" nav-item"><a href="{{url('user-company')}}"><i class="material-icons">card_travel</i><span class="menu-title" data-i18n="Order">Perusahaan</span></a>
                </li>
                <li class=" nav-item"><a href="{{url('user-proposals')}}"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="Order">Proposal</span></a>

                <!-- <li class=" nav-item"><a href="#"><i class="material-icons">straighten</i><span class="menu-title" data-i18n="Material Components">Material Components</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="material-component-buttons.html"><i class="material-icons"></i><span data-i18n="Buttons">Buttons</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-cards.html"><i class="material-icons"></i><span data-i18n="Cards">Cards</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-chips.html"><i class="material-icons"></i><span data-i18n="Chips">Chips</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-datatables.html"><i class="material-icons"></i><span data-i18n="Data tables">Data tables</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-dialogs.html"><i class="material-icons"></i><span data-i18n="Dialogs">Dialogs</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-elevation-shadows.html"><i class="material-icons"></i><span data-i18n="Elevation Shadows">Elevation Shadows</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-expansion-panels.html"><i class="material-icons"></i><span data-i18n="Expansion Panels">Expansion Panels</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-icons.html"><i class="material-icons"></i><span data-i18n="Icons">Icons</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-menu.html"><i class="material-icons"></i><span data-i18n="Menu">Menu</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-navigation.html"><i class="material-icons"></i><span data-i18n="Navigation Drawer">Navigation Drawer</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-pickers.html"><i class="material-icons"></i><span data-i18n="Pickers">Pickers</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-progress.html"><i class="material-icons"></i><span data-i18n="Progress">Progress</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-selection-controls.html"><i class="material-icons"></i><span data-i18n="Selection Controls">Selection Controls</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-snackbars.html"><i class="material-icons"></i><span data-i18n="Snackbars">Snackbars</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-steppers.html"><i class="material-icons"></i><span data-i18n="Steppers">Steppers</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-tabs.html"><i class="material-icons"></i><span data-i18n="Tabs">Tabs</span></a>
                        </li>
                        <li><a class="menu-item" href="material-component-textfields.html"><i class="material-icons"> </i><span data-i18n="Text fields">Text fields</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">view_list</i><span class="menu-title" data-i18n="Components">Components</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="component-alerts.html"><i class="material-icons"></i><span data-i18n="Alerts">Alerts</span></a>
                        </li>
                        <li><a class="menu-item" href="component-callout.html"><i class="material-icons"></i><span data-i18n="Callout">Callout</span></a>
                        </li>
                        <li><a class="menu-item" href="component-buttons-basic.html"><i class="material-icons"></i><span data-i18n="Buttons">Buttons</span></a>
                        </li>
                        <li><a class="menu-item" href="component-carousel.html"><i class="material-icons"></i><span data-i18n="Carousel">Carousel</span></a>
                        </li>
                        <li><a class="menu-item" href="component-collapse.html"><i class="material-icons"></i><span data-i18n="Collapse">Collapse</span></a>
                        </li>
                        <li><a class="menu-item" href="component-dropdowns.html"><i class="material-icons"></i><span data-i18n="Dropdowns">Dropdowns</span></a>
                        </li>
                        <li><a class="menu-item" href="component-list-group.html"><i class="material-icons"></i><span data-i18n="List Group">List Group</span></a>
                        </li>
                        <li><a class="menu-item" href="component-modals.html"><i class="material-icons"></i><span data-i18n="Modals">Modals</span></a>
                        </li>
                        <li><a class="menu-item" href="component-pagination.html"><i class="material-icons"></i><span data-i18n="Pagination">Pagination</span></a>
                        </li>
                        <li><a class="menu-item" href="component-navs-component.html"><i class="material-icons"></i><span data-i18n="Navs Component">Navs Component</span></a>
                        </li>
                        <li><a class="menu-item" href="component-tabs-component.html"><i class="material-icons"></i><span data-i18n="Tabs Component">Tabs Component</span></a>
                        </li>
                        <li><a class="menu-item" href="component-pills-component.html"><i class="material-icons"></i><span data-i18n="Pills Component">Pills Component</span></a>
                        </li>
                        <li><a class="menu-item" href="component-tooltips.html"><i class="material-icons"></i><span data-i18n="Tooltips">Tooltips</span></a>
                        </li>
                        <li><a class="menu-item" href="component-popovers.html"><i class="material-icons"></i><span data-i18n="Popovers">Popovers</span></a>
                        </li>
                        <li><a class="menu-item" href="component-badges.html"><i class="material-icons"></i><span data-i18n="Badges">Badges</span></a>
                        </li>
                        <li><a class="menu-item" href="component-pill-badges.html"><i class="material-icons"></i><span>Pill Badges</span></a>
                        </li>
                        <li><a class="menu-item" href="component-progress.html"><i class="material-icons"></i><span data-i18n="Progress">Progress</span></a>
                        </li>
                        <li><a class="menu-item" href="component-media-objects.html"><i class="material-icons"></i><span data-i18n="Media Objects">Media Objects</span></a>
                        </li>
                        <li><a class="menu-item" href="component-scrollable.html"><i class="material-icons"></i><span data-i18n="Scrollable">Scrollable</span></a>
                        </li>
                        <li><a class="menu-item" href="component-spinners.html"><i class="material-icons"></i><span data-i18n="Spinners">Spinners</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">lock_outline</i><span class="menu-title" data-i18n="Authentication">Authentication</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="login-with-bg-image.html" target="_blank"><i class="material-icons"></i><span>Login</span></a>
                        </li>
                        <li><a class="menu-item" href="register-with-bg-image.html" target="_blank"><i class="material-icons"></i><span>SignIn</span></a>
                        </li>
                        <li><a class="menu-item" href="recover-password.html" target="_blank"><i class="material-icons"></i><span>Forgot Password</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">grid_on</i><span class="menu-title" data-i18n="Form Layouts">Form Layouts</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="form-layout-basic.html"><i class="material-icons"></i><span data-i18n="Basic Forms">Basic Forms</span></a>
                        </li>
                        <li><a class="menu-item" href="form-layout-horizontal.html"><i class="material-icons"></i><span data-i18n="Horizontal Forms">Horizontal Forms</span></a>
                        </li>
                        <li><a class="menu-item" href="form-layout-hidden-labels.html"><i class="material-icons"></i><span data-i18n="Hidden Labels">Hidden Labels</span></a>
                        </li>
                        <li><a class="menu-item" href="form-layout-form-actions.html"><i class="material-icons"></i><span data-i18n="Form Actions">Form Actions</span></a>
                        </li>
                        <li><a class="menu-item" href="form-layout-row-separator.html"><i class="material-icons"></i><span data-i18n="Row Separator">Row Separator</span></a>
                        </li>
                        <li><a class="menu-item" href="form-layout-bordered.html"><i class="material-icons"></i><span data-i18n="Bordered">Bordered</span></a>
                        </li>
                        <li><a class="menu-item" href="form-layout-striped-rows.html"><i class="material-icons"></i><span data-i18n="Striped Rows">Striped Rows</span></a>
                        </li>
                        <li><a class="menu-item" href="form-layout-striped-labels.html"><i class="material-icons"></i><span data-i18n="Striped Labels">Striped Labels</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">linear_scale</i><span class="menu-title" data-i18n="Form Wizard">Form Wizard</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="form-wizard-circle-style.html"><i class="material-icons"></i><span data-i18n="Circle Style">Circle Style</span></a>
                        </li>
                        <li><a class="menu-item" href="form-wizard-notification-style.html"><i class="material-icons"></i><span data-i18n="Notification Style">Notification Style</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="Bootstrap Tables">Bootstrap Tables</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="table-basic.html"><i class="material-icons"></i><span data-i18n="Basic Tables">Basic Tables</span></a>
                        </li>
                        <li><a class="menu-item" href="table-border.html"><i class="material-icons"></i><span data-i18n="Table Border">Table Border</span></a>
                        </li>
                        <li><a class="menu-item" href="table-sizing.html"><i class="material-icons"></i><span data-i18n="Table Sizing">Table Sizing</span></a>
                        </li>
                        <li><a class="menu-item" href="table-styling.html"><i class="material-icons"></i><span data-i18n="Table Styling">Table Styling</span></a>
                        </li>
                        <li><a class="menu-item" href="table-components.html"><i class="material-icons"></i><span data-i18n="Table Components">Table Components</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">show_chart</i><span class="menu-title" data-i18n="Chartjs">Chartjs</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="chartjs-line-charts.html"><i class="material-icons"></i><span data-i18n="Line charts">Line charts</span></a>
                        </li>
                        <li><a class="menu-item" href="chartjs-bar-charts.html"><i class="material-icons"></i><span data-i18n="Bar charts">Bar charts</span></a>
                        </li>
                        <li><a class="menu-item" href="chartjs-pie-doughnut-charts.html"><i class="material-icons"></i><span data-i18n="Pie &amp; Doughnut charts">Pie &amp; Doughnut charts</span></a>
                        </li>
                        <li><a class="menu-item" href="chartjs-scatter-charts.html"><i class="material-icons"></i><span data-i18n="Scatter charts">Scatter charts</span></a>
                        </li>
                        <li><a class="menu-item" href="chartjs-polar-radar-charts.html"><i class="material-icons"></i><span data-i18n="Polar &amp; Radar charts">Polar &amp; Radar charts</span></a>
                        </li>
                        <li><a class="menu-item" href="chartjs-advance-charts.html"><i class="material-icons"></i><span data-i18n="Advance charts">Advance charts</span></a>
                        </li>
                    </ul>
                </li> -->
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->