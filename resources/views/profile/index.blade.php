@extends('layouts.app')

@section('content')


                <div class="content-header-right col-md-6 col-12">
                    <div class="btn-group float-md-right">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-icons">Timesheet</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">

                                        <div class="card-text">
                                            <p>This form shows the use of icons with form controls. Define the position of the icon using <code>has-icon-left</code> or <code>has-icon-right</code> class. Use <code>icon-*</code> class to define the icon for the form control. See Icons sections for the list of icons you can use. </p>
                                        </div>

                                        <form class="form" action="{{url('user-profile')}}" method="post">
                                            @csrf
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="timesheetinput1">Name</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="timesheetinput1" class="form-control" value="{{$user->name ?? ''}}" placeholder="Full Name" name="name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput2">Email</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="email" id="timesheetinput2" class="form-control" value="{{$user->email ?? ''}}" placeholder="Email" name="email">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">KTP</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$user->ktp ?? ''}}" placeholder="KTP" name="ktp">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">NPWP</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$user->npwp ?? ''}}" placeholder="NPWP" name="npwp">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">HP</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$user->hp ?? ''}}"  placeholder="HP" name="hp">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                               

                                                <div class="form-group">
                                                    <label for="timesheetinput7">Photo</label>
                                                    <div class="position-relative has-icon-left">
                                                    <input type="file" id="timesheetinput3" class="form-control"  placeholder="Photo" name="photo">
                                                        <div class="form-control-position">
                                                            <i class="ft-file"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions right">
                                                <button type="button" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </section>
                <!-- // Basic form layout section end -->

@endsection