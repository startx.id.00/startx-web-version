<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'address' => $this->faker->word,
        'nib' => $this->faker->word,
        'npwp' => $this->faker->word,
        'akta' => $this->faker->word,
        'sk' => $this->faker->word,
        'izin' => $this->faker->word,
        'image' => $this->faker->word,
        'rekening' => $this->faker->word,
        'telp' => $this->faker->word,
        'status' => $this->faker->randomDigitNotNull,
        'category_id' => $this->faker->randomDigitNotNull,
        'map' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
