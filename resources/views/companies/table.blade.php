<div class="table-responsive-sm">
    <table class="table table-striped" id="companies-table">
        <thead>
            <tr>
                <th>User</th>
        <th>Name</th>
        <th>Status</th>
        <th>Category</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($companies as $company)
            <tr>
                <td>{{ $company->user->name }}</td>
            <td>{{ $company->name }}</td>
            <td>{{ $company->status = 1 ? "Review" : "Approved" }}</td>
            <td>{{ $company->category->name }}</td>
                <td>
                    {!! Form::open(['route' => ['companies.destroy', $company->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('companies.show', [$company->id]) }}" class='btn btn-success'>Tampilkan</a>
                        <a href="{{ route('companies.edit', [$company->id]) }}" class='btn btn-info'>Rubah</a>
                        {!! Form::button('Hapus', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>