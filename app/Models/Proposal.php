<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Proposal
 * @package App\Models
 * @version November 29, 2020, 2:52 pm UTC
 *
 * @property integer $user_id
 * @property integer $company_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $summary
 * @property integer $price
 * @property integer $shares
 * @property integer $period
 * @property string $due
 * @property string $code
 * @property string $image
 * @property string $file
 * @property integer $status
 */
class Proposal extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'proposals';
    

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User')->withDefault();
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category')->withDefault();
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company')->withDefault();
    }

    public $fillable = [
        'user_id',
        'company_id',
        'title',
        'slug',
        'description',
        'summary',
        'price',
        'shares',
        'period',
        'due',
        'code',
        'image',
        'file',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'company_id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'summary' => 'string',
        'price' => 'integer',
        'shares' => 'integer',
        'period' => 'integer',
        'due' => 'date',
        'code' => 'string',
        'image' => 'string',
        'file' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'company_id' => 'required',
        'title' => 'required',
        'description' => 'required',
        'summary' => 'required',
        'price' => 'numeric',
        'shares' => 'numeric',
        'period' => 'numeric',
        'due' => 'required',
        'image' => 'required',
        'file' => 'required',
        'status' => 'required'
    ];

    
}
