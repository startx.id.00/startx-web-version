<!DOCTYPE html>
<html lang="en">

@include('template.head')

<body>

    @include('template.menu')

    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="banner_content text-center">
                            <h2>About Us</h2>
                            <div class="page_link">
                                <a href="index.html">Home</a>
                                <a href="about-us.html">About Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================ Start About Area =================-->
    <section class="about_area section_gap">
        <div class="container">
            <div class="row h_blog_item">
                <div class="col-lg-6">
                    <div class="h_blog_img">
                        <img class="img-fluid" src="{{asset('edustage/img/about.png')}}" alt="" />
                    </div>
                    <div class="h_blog_img">
                        <img class="img-fluid" src="{{asset('edustage/img/about.png')}}" alt="" />
                    </div>
                    <div class="h_blog_img">
                        <img class="img-fluid" src="{{asset('edustage/img/about.png')}}" alt="" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="h_blog_text">
                        <div class="h_blog_text_inner left right">
                            <h4>Selamat datang di Startx,</h4>
                            <p>Platform Equity Crowdfunding pertama yang berizin dan diawasi Otoritas Jasa Keuangan
                                berdasarkan Surat Keputusan Nomor:.<br></p>
                            <hr>
                            <p><strong>Apa itu Equity Crowdfunding?</strong></p>
                            <p>Equity Crowdfunding adalah istilah untuk layanan urun dana melalui penawaran saham
                                berbasis teknologi informasi.</p>
                            <p>Sederhananya..</p>
                            <p>Startx adalah penghubung antara pelaku bisnis yang ingin mengembangkan usahanya dengan
                                masyarakat yang ingin memiliki bisnis (investor).<br><span
                                    style="font-size: 12pt;">Menarik, bukan?</span></p>
                            <p><strong>Bagi Anda pelaku bisnis</strong></p>
                            <p>Di Startx Anda bisa mendapatkan pendanaan untuk mengembangkan bisnis Anda dengan cara
                                menawarkan sebagian saham bisnis Anda kepada investor.</p>
                            <p>Tanpa bunga, tanpa denda, cukup bagi dividen hasil usaha saja.</p>
                            <p><strong>Bagi Anda yang INGIN memiliki bisnis</strong></p>
                            <p>Startx membantu Anda untuk menseleksi bisnis unggulan, Anda dapat berinvestasi membeli
                                saham pada sebuah bisnis dan menikmati passive income dari dividen dari hasil usaha
                                tersebut.</p>
                            <p>Bisnis sudah berjalan dan menguntungkan dan Anda tidak perlu ‘ribet’ ikut mengurus
                                operasional bisnisnya.<br></p>
                            <p>Kerjasama antara pelaku bisnis dan investor ini dilakukah dengan sistem bagi dividen
                                hasil usaha.<br></p>
                            <p>Dengan pola ini, kami berharap semakin banyak bisnis UKM yang mampu Naik Kelas. Saat
                                bisnis semakin berkembang, manfaatnya dirasakan oleh lebih banyak orang.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End About Area =================-->

    <!--================ Start Feature Area =================-->
    <section class="feature_area section_gap_top title-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="main_title">
                        <h2 class="mb-3 text-white">Awesome Feature</h2>
                        <p>
                            Replenish man have thing gathering lights yielding shall you
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single_feature">
                        
                        <div class="desc">
                            <h4 class="mt-3 mb-2">Ajukan Permodalan</h4>
                            <p>
                                One make creepeth, man bearing theira firmament won't great
                                heaven
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single_feature">
                        
                        <div class="desc">
                            <h4 class="mt-3 mb-2">Berinvestasi</h4>
                            <p>
                                One make creepeth, man bearing theira firmament won't great
                                heaven
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single_feature">
                        
                        <div class="desc">
                            <h4 class="mt-3 mb-2">Hubungi Kami</h4>
                            <p>
                                One make creepeth, man bearing theira firmament won't great
                                heaven
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Feature Area =================-->

    <!--================ Start Testimonial Area =================-->
    <div class="testimonial_area section_gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="main_title">
                        <h2 class="mb-3">Client say about me</h2>
                        <p>
                            Replenish man have thing gathering lights yielding shall you
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="testi_slider owl-carousel">
                    <div class="testi_item">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <img src="{{asset('edustage/img/testimonials/t1.jpg')}}" alt="" />
                            </div>
                            <div class="col-lg-8">
                                <div class="testi_text">
                                    <h4>Elite Martin</h4>
                                    <p>
                                        Him, made can't called over won't there on divide there
                                        male fish beast own his day third seed sixth seas unto.
                                        Saw from
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi_item">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <img src="{{asset('edustage/img/testimonials/t2.jpg')}}" alt="" />
                            </div>
                            <div class="col-lg-8">
                                <div class="testi_text">
                                    <h4>Davil Saden</h4>
                                    <p>
                                        Him, made can't called over won't there on divide there
                                        male fish beast own his day third seed sixth seas unto.
                                        Saw from
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi_item">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <img src="{{asset('edustage/img/testimonials/t1.jpg')}}" alt="" />
                            </div>
                            <div class="col-lg-8">
                                <div class="testi_text">
                                    <h4>Elite Martin</h4>
                                    <p>
                                        Him, made can't called over won't there on divide there
                                        male fish beast own his day third seed sixth seas unto.
                                        Saw from
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi_item">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <img src="{{asset('edustage/img/testimonials/t2.jpg')}}" alt="" />
                            </div>
                            <div class="col-lg-8">
                                <div class="testi_text">
                                    <h4>Davil Saden</h4>
                                    <p>
                                        Him, made can't called over won't there on divide there
                                        male fish beast own his day third seed sixth seas unto.
                                        Saw from
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi_item">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <img src="{{asset('edustage/img/testimonials/t1.jpg')}}" alt="" />
                            </div>
                            <div class="col-lg-8">
                                <div class="testi_text">
                                    <h4>Elite Martin</h4>
                                    <p>
                                        Him, made can't called over won't there on divide there
                                        male fish beast own his day third seed sixth seas unto.
                                        Saw from
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi_item">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <img src="{{asset('edustage/img/testimonials/t2.jpg')}}" alt="" />
                            </div>
                            <div class="col-lg-8">
                                <div class="testi_text">
                                    <h4>Davil Saden</h4>
                                    <p>
                                        Him, made can't called over won't there on divide there
                                        male fish beast own his day third seed sixth seas unto.
                                        Saw from
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================ End Testimonial Area =================-->

    @include('template.footer')

</body>

</html>
