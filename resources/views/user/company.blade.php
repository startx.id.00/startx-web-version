@extends('layouts.app')

@section('content')


                <div class="content-header-right col-md-6 col-12">
                    <div class="btn-group float-md-right">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="basic-layout-icons">Timesheet</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">

                                        <div class="card-text">
                                            <p>This form shows the use of icons with form controls. Define the position of the icon using <code>has-icon-left</code> or <code>has-icon-right</code> class. Use <code>icon-*</code> class to define the icon for the form control. See Icons sections for the list of icons you can use. </p>
                                        </div>

                                        <form class="form" action="{{url('user-company')}}" method="post">
                                            @csrf
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="timesheetinput1">Nama</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="timesheetinput1" class="form-control" value="{{$company->name ?? ''}}" placeholder="Nama Perusahaan" name="name">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput1">Kategori</label>
                                                    <div class="position-relative has-icon-left">
                                                        {{$company->category_id}}
                                                        <select name="category_id" id="" class="form-control">
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->id}}" <?php if($company->category_id == $category->id){ echo "checked"; } ?> >{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput1">address</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="timesheetinput1" class="form-control" value="{{$company->address ?? ''}}" placeholder="Alamat" name="address">
                                                        <div class="form-control-position">
                                                            <i class="ft-user"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">npwp</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$company->npwp ?? ''}}" placeholder="NPWP" name="npwp">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">akta</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$company->akta ?? ''}}" placeholder="akta" name="akta">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">sk</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$company->sk ?? ''}}" placeholder="sk" name="sk">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">izin</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$company->izin ?? ''}}" placeholder="izin" name="izin">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">rekening</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$company->rekening ?? ''}}" placeholder="rekening" name="rekening">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">telp</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$company->telp ?? ''}}"  placeholder="telp" name="telp">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timesheetinput3">map</label>
                                                    <div class="position-relative has-icon-left">
                                                        <input type="name" id="timesheetinput3" class="form-control" value="{{$company->map ?? ''}}"  placeholder="map" name="map">
                                                        <div class="form-control-position">
                                                            <i class="ft-message-square"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                               

                                                <div class="form-group">
                                                    <label for="timesheetinput7">image</label>
                                                    <div class="position-relative has-icon-left">
                                                    <input type="file" id="timesheetinput3" class="form-control"  placeholder="image" name="image">
                                                        <div class="form-control-position">
                                                            <i class="ft-file"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions right">
                                                <button type="button" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </section>
                <!-- // Basic form layout section end -->

@endsection