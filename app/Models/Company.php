<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Company
 * @package App\Models
 * @version November 29, 2020, 2:11 pm UTC
 *
 * @property integer $user_id
 * @property string $name
 * @property string $address
 * @property string $nib
 * @property string $npwp
 * @property string $akta
 * @property string $sk
 * @property string $izin
 * @property string $image
 * @property string $rekening
 * @property string $telp
 * @property integer $status
 * @property integer $category_id
 */
class Company extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'companies';
    

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User')->withDefault();
    }
    
    public function category()
    {
        return $this->belongsTo('App\Models\Category')->withDefault();
    }

    public $fillable = [
        'user_id',
        'name',
        'address',
        'nib',
        'npwp',
        'akta',
        'sk',
        'izin',
        'image',
        'rekening',
        'telp',
        'status',
        'category_id',
        'map'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'nib' => 'string',
        'npwp' => 'string',
        'akta' => 'string',
        'sk' => 'string',
        'izin' => 'string',
        'image' => 'string',
        'rekening' => 'string',
        'telp' => 'string',
        'status' => 'integer',
        'category_id' => 'integer',
        'map' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'name' => 'required',
        'address' => 'required',
        'image' => 'required',
        'telp' => 'required',
        'status' => 'required',
        'category_id' => 'required'
    ];

    
}
