<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;


class UserProfileController extends Controller
{
    public function index()
    {
        $user = User::where('id',Auth::user()->id)->first();

        return view('profile.index')->with(['user'=>$user]);
    }    
    
    public function store(Request $request)
    {
        $user = User::where('id',Auth::user()->id)->first();
        $user->name = $request->name;
        $user->email    = $request->email;
        $user->ktp  = $request->ktp;
        $user->npwp = $request->npwp;
        $user->hp   = $request->hp;
        if(isset($request->photo)){
            $user->photo    = $request->photo;
        }
        $user->save();

        // $user = User::where('id',Auth::user()->id)->first();
        // return view('profile.index')->with(['user'=>$user]);
        return redirect('user-profile');
    }    
}
