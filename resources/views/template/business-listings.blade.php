<!DOCTYPE html>
<html lang="en">

@include('template.head')

  <body>
    
    @include('template.menu')


    <!--================ Start Home Banner Area =================-->
    <section class="home_banner_area">
      <div class="banner_inner">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="banner_content text-center">
                <p class="text-uppercase">
                  Make Your Money Work For You
                </p>
                <h2 class="text-uppercase mt-4 mb-5">
                MARKETPLACE INVESTASI TERPERCAYA
                </h2>
                <div>
                <a href="{{url('business-listings')}}" class="primary-btn2 mb-3 mb-sm-0">Berinvetasi</a>
                  <a href="#" class="primary-btn ml-sm-3 ml-0">Ajukan Permodalan</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--================ End Home Banner Area =================-->

    <!--================Blog Area =================-->
    <section class="blog_area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog_left_sidebar">
                        @foreach($proposals as $proposal)
                        <article class="row blog_item">
                            <div class="col-md-3">
                                <div class="blog_info text-right">
                                    <div class="post_tag">
                                        <a href="#">{{$proposal->company->category->name}}</a>
                                        <!-- <a class="active" href="#">Technology,</a>
                                        <a href="#">Politics,</a>
                                        <a href="#">Lifestyle</a> -->
                                    </div>
                                    <ul class="blog_meta list">
                                        <!-- <li><a href="#">Mark wiens<i class="ti-user"></i></a></li> -->
                                        <li><a href="#">{{ \Carbon\Carbon::parse( $proposal->due )->diffForHumans() }}<i class="ti-calendar"></i></a></li>
                                        <li><a href="#">Rp {{number_format($proposal->price * $proposal->shares)}}<i class="ti-money"></i></a></li>
                                        <li><a href="#">{{$proposal->period}} Bulan<i class="ti-timer"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="blog_post">
                                <img class="img-fluid" src="{{asset($proposal->image)}}" alt="" 
                  onerror="this.onerror=null;this.src='https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg';" />
                                    <div class="blog_details">
                                        <a href="{{url('proposal/'.$proposal->slug)}}">
                                            <h2>{{$proposal->title}}</h2>
                                        </a>
                                        <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">75%</div>
                  </div> 
                  <br>
                                        <p> {{$proposal->summary}}</p>
                                        <a href="{{url('proposal/'.$proposal->slug)}}" class="blog_btn">View More</a>
                                    </div>
                                </div>
                            </div>
                        </article>
                        @endforeach
                        
                        <nav class="blog-pagination justify-content-center d-flex">
                        {{$proposals->links("pagination::bootstrap-4")}}
                            
                        </nav>
                        
                    </div>
                </div>
               
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <div class="input-group">
                              <form action="{{url('search')}}" method="post">
                                @csrf
                                <input type="text" name="q" class="form-control" placeholder="Search Posts">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="ti-search"></i></button>
                                </span>
                              </form>
                                
                            </div><!-- /input-group -->
                            <div class="br"></div>
                        </aside>
                        <!-- <aside class="single_sidebar_widget author_widget">
                            <img class="author_img rounded-circle" src="img/blog/author.png" alt="">
                            <h4>Charlie Barber</h4>
                            <p>Senior blog writer</p>
                            <div class="social_icon">
                                <a href="#"><i class="ti-facebook"></i></a>
                                <a href="#"><i class="ti-twitter"></i></a>
                                <a href="#"><i class="ti-github"></i></a>
                                <a href="#"><i class="ti-linkedin"></i></a>
                            </div>
                            <p>Boot camps have its supporters andit sdetractors. Some people do not understand why you
                                should have to spend money on boot camp when you can get. Boot camps have itssuppor
                                ters andits detractors.</p>
                            <div class="br"></div>
                        </aside> -->
                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title">Popular Posts</h3>
                            @foreach($populars as $popular)
                            <div class="media post_item">
                                <img src="{{asset($proposal->image)}}" width="100px" alt="" 
                  onerror="this.onerror=null;this.src='https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg';">
                                <div class="media-body">
                                    <a href="blog-details.html">
                                        <h3>{{$proposal->title}}</h3>
                                    </a>
                                    <p>{{ \Carbon\Carbon::parse( $proposal->due )->diffForHumans() }}</p>
                                </div>
                            </div>
                            @endforeach
                            
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget ads_widget">
                            <a href="#"><img class="img-fluid" src="img/blog/add.jpg" alt=""></a>
                            <div class="br"></div>
                        </aside>
                        <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title">Proposals Catgories</h4>
                            <ul class="list cat-list">
                                @foreach($categories as $category)  
                                <li>
                                    <a href="#" class="d-flex justify-content-between">
                                        <p>{{$category->name}}</p>
                                        <!-- <p>37</p> -->
                                    </a>
                                </li>
                                @endforeach
                                
                            </ul>
                            <div class="br"></div>
                        </aside>
                        <aside class="single-sidebar-widget newsletter_widget">
                            <h4 class="widget_title">Newsletter</h4>
                            <p>
                                Here, I focus on a range of items and features that we use in life without
                                giving them a second thought.
                            </p>
                            <div class="form-group d-flex flex-row">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="ti-email" aria-hidden="true"></i></div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email"
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'">
                                </div>
                                <a href="#" class="bbtns">Subcribe</a>
                            </div>
                            <p class="text-bottom">You can unsubscribe at any time</p>
                            <div class="br"></div>
                        </aside>
                        <!-- <aside class="single-sidebar-widget tag_cloud_widget">
                            <h4 class="widget_title">Tag Clouds</h4>
                            <ul class="list">
                                <li><a href="#">Technology</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Architecture</a></li>
                                <li><a href="#">Fashion</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Technology</a></li>
                                <li><a href="#">Lifestyle</a></li>
                                <li><a href="#">Art</a></li>
                                <li><a href="#">Adventure</a></li>
                                <li><a href="#">Food</a></li>
                                <li><a href="#">Lifestyle</a></li>
                                <li><a href="#">Adventure</a></li>
                            </ul>
                        </aside> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->

    @include('template.footer')

  </body>
</html>