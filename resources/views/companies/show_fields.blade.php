<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $company->user_id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $company->name }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $company->address }}</p>
</div>

<!-- Nib Field -->
<div class="form-group">
    {!! Form::label('nib', 'Nib:') !!}
    <p>{{ $company->nib }}</p>
</div>

<!-- Npwp Field -->
<div class="form-group">
    {!! Form::label('npwp', 'Npwp:') !!}
    <p>{{ $company->npwp }}</p>
</div>

<!-- Akta Field -->
<div class="form-group">
    {!! Form::label('akta', 'Akta:') !!}
    <p>{{ $company->akta }}</p>
</div>

<!-- Sk Field -->
<div class="form-group">
    {!! Form::label('sk', 'Sk:') !!}
    <p>{{ $company->sk }}</p>
</div>

<!-- Izin Field -->
<div class="form-group">
    {!! Form::label('izin', 'Izin:') !!}
    <p>{{ $company->izin }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{{ $company->image }}</p>
</div>

<!-- Rekening Field -->
<div class="form-group">
    {!! Form::label('rekening', 'Rekening:') !!}
    <p>{{ $company->rekening }}</p>
</div>

<!-- Telp Field -->
<div class="form-group">
    {!! Form::label('telp', 'Telp:') !!}
    <p>{{ $company->telp }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $company->status }}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{{ $company->category_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $company->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $company->updated_at }}</p>
</div>

