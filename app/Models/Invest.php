<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invest extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User')->withDefault();
    }
    
    public function company()
    {
        return $this->belongsTo('App\Models\Company')->withDefault();
    }

    public function proposal()
    {
        return $this->belongsTo('App\Models\Proposal')->withDefault();
    }
}
