<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvestController;
// use App\Http\Controllers\SubmissionController;

use App\Http\Controllers\ProposalController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CompanyController;

use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\UserCompanyController;
use App\Http\Controllers\UserProposalController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('template.index');
// });
// Route::get('/about', function () {
//     return view('template.about-us');
// });

Route::get('/', [DashboardController::class, 'index']);
Route::get('/about', [DashboardController::class, 'about']);
Route::get('/question', [DashboardController::class, 'question']);
Route::get('/business-listings', [DashboardController::class, 'businessListings']);
Route::post('/search', [DashboardController::class, 'search']);

Route::get('/proposal/{slug}', [DashboardController::class, 'proposal']);



Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::post('/subscription', [HomeController::class, 'subscription']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('admin.index');
})->name('dashboard');

// Route::get('/test', function () {
//     return view('admin.index');
// });
// Route::get('/testlogin', function () {
//     return view('admin.login-with-bg-image');
// });
// Route::get('/testregister', function () {
//     return view('admin.register-with-bg-image');
// });

Route::resource('categories', CategoryController::class);
Route::resource('companies', CompanyController::class);
Route::resource('proposals', ProposalController::class);

Route::middleware(['auth'])->group(function () {

    Route::get('/invest/{slug}', [InvestController::class, 'index']);
    Route::get('/submission', [UserProposalController::class, 'create']);

    Route::get('user-profile', [UserProfileController::class, 'index']);
    Route::post('user-profile', [UserProfileController::class, 'store']);

    Route::get('user-company', [UserCompanyController::class, 'index']);
    Route::post('user-company', [UserCompanyController::class, 'store']);

    Route::get('user-proposals', [UserProposalController::class, 'index']);
    Route::get('user-proposals/create', [UserProposalController::class, 'create']);
    Route::get('user-proposals/{id}', [UserProposalController::class, 'edit']);
    Route::post('user-proposals', [UserProposalController::class, 'store']);

});