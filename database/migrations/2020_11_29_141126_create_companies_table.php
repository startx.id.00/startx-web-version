<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('nib')->nullable();
            $table->string('npwp')->nullable();
            $table->string('akta')->nullable();
            $table->string('sk')->nullable();
            $table->string('izin')->nullable();
            $table->string('image');
            $table->string('rekening')->nullable();
            $table->string('telp');
            $table->integer('status');
            $table->bigInteger('category_id')->unsigned();
            $table->string('map');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
