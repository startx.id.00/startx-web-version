<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proposal;
use Auth;

class UserProposalController extends Controller
{
    public function index()
    {
        $proposals = Proposal::where('user_id',Auth::user()->id)->get();
        return view('proposals.index')->with(['proposals' => $proposals]);
    }

    public function create()
    {
        return view('proposals.create');
    }

    public function list()
    {
        return view('user.proposal');
    }
}
