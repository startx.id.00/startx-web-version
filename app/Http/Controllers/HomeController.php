<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscription;
use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function subscription(Request $request)
    {
        $subcribe = new Subscription;
        $subcribe->email = $request->email;
        $subcribe->name = $request->name ?? null;
        $subcribe->telp = $request->telp ?? null;
        $subcribe->status = 0;
        $subcribe->save();
        
        return Redirect::back();
        // return view('home');
    }
}
