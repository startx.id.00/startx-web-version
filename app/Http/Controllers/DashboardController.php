<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proposal;
use App\Models\Category;
use App\Models\Testimony;

class DashboardController extends Controller
{
    public function index()
    {
        
        $proposals = Proposal::where('status',1)->get();
        $testimonies = Testimony::where('status',1)->get();
        $menu = "home";
        return view('template.index')->with(["menu" => $menu,
                                            "proposals" => $proposals,
                                            "testimonies" => $testimonies]);
    }

    public function proposal($slug)
    {
        $proposal = Proposal::where('slug',$slug)->first();
        $menu = "home";
        return view('template.proposal-detail')->with(["menu" => $menu,
                                            "proposal" => $proposal]);
    }


    public function about()
    {
        $menu = "about";
        return view('template.about-us')->with(["menu" => $menu]);
    }

    public function question()
    {
        $menu = "question";
        $testimonies = Testimony::where('status',1)->get();
        return view('template.question')->with(["menu" => $menu,
                                                "testimonies" => $testimonies]);
    }

    public function businessListings()
    {
        $proposals = Proposal::where('status',1)->paginate();
        $populars = Proposal::where('status',1)->paginate();
        $categories = Category::get();
        $menu = "list";
        return view('template.business-listings')->with(["menu" => $menu,
                                                        "proposals" => $proposals,
                                                        "categories" => $categories,
                                                        "populars" => $populars]);
    }

    public function search(Request $request)
    {
        $search =  $request->input('q');
        if($search!=""){
            $proposals = Proposal::where('status',1)->where(function ($query) use ($search){
                $query->where('title', 'like', '%'.$search.'%')
                    ->orWhere('description', 'like', '%'.$search.'%');
            })
            ->paginate(2);
            $proposals->appends(['q' => $search]);
        }
        else{
            $proposals = Proposal::where('status',1)->paginate();
        }

        $populars = Proposal::where('status',1)->paginate();
        $categories = Category::get();

        $menu = "list";
        return view('template.business-listings')->with(["menu" => $menu,
                                                        "proposals" => $proposals,
                                                        "categories" => $categories,
                                                        "populars" => $populars]);

    }
}
