  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link rel="icon" href="{{asset('edustage/img/favicon.png')}}" type="image/png" />
    <title>Startx</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('edustage/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('edustage/css/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('edustage/css/themify-icons.css')}}" />
    <link rel="stylesheet" href="{{asset('edustage/vendors/owl-carousel/owl.carousel.min.css')}}" />
    <link rel="stylesheet" href="{{asset('edustage/vendors/nice-select/css/nice-select.css')}}" />
    <!-- main css -->
    <link rel="stylesheet" href="{{asset('edustage/css/style.css')}}" />
  </head>