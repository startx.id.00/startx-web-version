<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Nib Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nib', 'Nib:') !!}
    {!! Form::text('nib', null, ['class' => 'form-control']) !!}
</div>

<!-- Npwp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('npwp', 'Npwp:') !!}
    {!! Form::text('npwp', null, ['class' => 'form-control']) !!}
</div>

<!-- Akta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('akta', 'Akta:') !!}
    {!! Form::text('akta', null, ['class' => 'form-control']) !!}
</div>

<!-- Sk Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sk', 'Sk:') !!}
    {!! Form::text('sk', null, ['class' => 'form-control']) !!}
</div>

<!-- Izin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('izin', 'Izin:') !!}
    {!! Form::text('izin', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
</div>

<!-- Rekening Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rekening', 'Rekening:') !!}
    {!! Form::text('rekening', null, ['class' => 'form-control']) !!}
</div>

<!-- Telp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telp', 'Telp:') !!}
    {!! Form::text('telp', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::text('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- map Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('map', 'Embed Map:') !!}
    {!! Form::text('map', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('companies.index') }}" class="btn btn-secondary">Cancel</a>
</div>
