    <!--================ Start footer Area  =================-->
<!--Div where the WhatsApp will be rendered-->  
<div id="WAButton"></div>  


    <footer class="footer-area section_gap">
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-md-6 single-footer-widget">
            <h4>Top Products</h4>
            <ul>
              <li><a href="#">Cara Berinvestasi</a></li>
              <li><a href="#">Cara Mengajukan Modal</a></li>
            </ul>
          </div>
          <div class="col-lg-2 col-md-6 single-footer-widget">
            <h4>Menu</h4>
            <ul>
              <li><a href="{{url('/')}}">Home</a></li>
              <li><a href="{{url('about')}}">Tentang Kami</a></li>
              <li><a href="{{url('question')}}">Pertanyaan</a></li>
              <li><a href="{url('business-listings')}}">Peluang Bisnis</a></li>
            </ul>
          </div>
          <!-- <div class="col-lg-2 col-md-6 single-footer-widget">
            <h4>Features</h4>
            <ul>
              <li><a href="#">Jobs</a></li>
              <li><a href="#">Brand Assets</a></li>
              <li><a href="#">Investor Relations</a></li>
              <li><a href="#">Terms of Service</a></li>
            </ul>
          </div> -->
          <div class="col-lg-4 col-md-6 single-footer-widget">
            <h4>Kontak Kami</h4>
            <p></p>
            <p>PT. Faris land Solution</p>
            <p>Jl. Pariwisata Baru Lekong Pituk, Tetebatu Selatan, Siklur, Lombok Timur</p>
            <p>WhatsApp : +62 878-3656-6062</p>
            <p>Email : cs@startx.id</p>
          </div>
          <div class="col-lg-4 col-md-6 single-footer-widget">
            <h4>Newsletter</h4>
            <p>You can trust us. we only send promo offers,</p>
            <div class="form-wrap">
              <form
                action="{{url('subscription')}}"
                method="post"
                class="form-inline"
              >
              @csrf
                <input
                  class="form-control"
                  name="email"
                  placeholder="Your Email Address"
                  required=""
                  type="email"
                />
                <button class="click-btn btn btn-default">
                  <span>subscribe</span>
                </button>
                <!-- <div style="position: absolute; left: -5000px;">
                  <input
                    name="b_36c4fd991d266f23781ded980_aefe40901a"
                    tabindex="-1"
                    value=""
                    type="text"
                  />
                </div> -->

                <div class="info"></div>
              </form>
            </div>
          </div>
        </div>
        <div class="row footer-bottom d-flex justify-content-between">
          <p class="col-lg-8 col-sm-12 footer-text m-0 text-white">

          </p>
          <div class="col-lg-4 col-sm-12 footer-social">
            <a href="#"><i class="ti-facebook"></i></a>
            <a href="#"><i class="ti-twitter"></i></a>
            <a href="#"><i class="ti-dribbble"></i></a>
            <a href="#"><i class="ti-linkedin"></i></a>
          </div>
        </div>
      </div>
    </footer>
    <!--================ End footer Area  =================-->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="{{asset('edustage/js/jquery-3.2.1.min.js')}}"></script> -->

<!--Jquery-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<!--Floating WhatsApp css-->
<link rel="stylesheet" href="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.css">
<!--Floating WhatsApp javascript-->
<script type="text/javascript" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.js"></script>



    <script src="{{asset('edustage/js/popper.js')}}"></script>
    <script src="{{asset('edustage/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('edustage/vendors/nice-select/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('edustage/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('edustage/js/owl-carousel-thumb.min.js')}}"></script>
    <script src="{{asset('edustage/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('edustage/js/mail-script.js')}}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{asset('edustage/js/gmaps.min.js')}}"></script>
    <script src="{{asset('edustage/js/theme.js')}}"></script>

    <script type="text/javascript">  
   $(function () {
           $('#WAButton').floatingWhatsApp({
               phone: '+62 878-3656-6062', //WhatsApp Business phone number
               headerTitle: 'Chat with us on WhatsApp!', //Popup Title
               popupMessage: 'Hello, how can we help you?', //Popup Message
               showPopup: true, //Enables popup display
               buttonImage: '<img src="https://cdn3.iconfinder.com/data/icons/social-media-special/256/whatsapp-512.png" />', //Button Image
               //headerColor: 'crimson', //Custom header color
               //backgroundColor: 'crimson', //Custom background button color
               position: "right" //Position: left | right

           });
       });
</script>  
<script>
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://startx-id.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>