<!DOCTYPE html>
<html lang="en">

@include('template.head')

<body>

    @include('template.menu')

    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="banner_content text-center">
                            <h4> <b> Common Question </b></h4>
                            <div class="page_link">
                                <a href="index.html">Home</a>
                                <a href="about-us.html">About Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about_area section_gap">
        <div class="container">



        <h2>Pertanyaan Yang Sering Ditanyakan</h2>
        <hr>
        <h3>Umum</h3>
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                <h4>Apa itu Startx ?</h4>
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                        <p>Startx adalah Platform Layanan Urun Dana Melalui Penawaran Saham Berbasis Teknologi Informasi (Equity Crowdfunding) yang telah berizin dan diawasi oleh Otoritas Jasa Keuangan (OJK)</p>
                        <p>Melalui Startx anda dapat mengambil peluang sebagai Pemodal dan Penerbit. Pada sisi Pemodal, Anda dapat membeli saham bisnis UKM yang kami tawarkan dan menikmati penghasilan dari dividen UKM yang Anda beli sahamnya tersebut.</p>
                        <p>Sedangkan dari sisi Penerbit, Anda dapat menawarkan saham dari bisnis UKM yang Anda miliki kepada para Pemodal yang terdaftar di Startx sehingga mendapatkan tambahan permodalan untuk pengembangan bisnis UKM Anda.</p>
                        <p>Selengkapnya tentang Startx juga dapat disimak dalam video berikut ini.</p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
                                aria-expanded="false" aria-controls="collapseThree">
                                <h4>Apa Peran Startx ?</h4>
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        Peran Startx dalam mempertemukan Pemodal dan Penerbit adalah :
                        <ul>
                            <li>Melakukan review terhadap bisnis calon Penerbit yang ingin mendapatkan pendanaan.</li>                        
                            <li>Menyediakan system penggalangan dana investasi (Equity Crowdfunding).</li>
                            <li>Menjadi penghubung antara Pemodal, Penerbit, dan regulator terkait.</li>
                            <li>Melakukan pengawasan terhadap berjalannya proses bisnis dalam layanan urun dana ini (baik dari sisi pemodal dan penerbit) agar dapat tetap berjalan sesuai koridor peraturan yang berlaku.</li>
                        </ul>

                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                aria-expanded="false" aria-controls="collapseTwo">
                                <h4>Kenapa Saya Harus Berinvestasi Kepada Bisnis UKM Melalui Startx ?</h4>
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        <p>Sebagian dari Anda mungkin berpikir “kalau berinvestasi pada bisnis UKM, kenapa tidak secara langsung saja?.</p>
                        <p>Betul, Anda bisa saja mencari pebisnis UKM yang bisnisnya bagus dan sedang berencana ekspansi. Anda bisa saja melakukan negosiasi sendiri dan kalau beruntung Anda bisa mendapatkan kesepakatan yang menarik untuk bekerja sama investasi dengan UKM tersebut. Namun Startx memberikan Anda 5 kemudahan bagi Anda untuk berinvestasi di bisnis UKM.</p>
                        <p>Yang Startx dapat tawarkan untuk Anda diantaranya :</p>
                        <ol>
                        <li>Ada banyak pilihan bisnis UKM.</li>
                        <p>Jika Anda ingin berinvestasi di banyak bisnis, maka Startx adalah tempat yang tepat. Anda tidak perlu mencari dan berkenalan dengan owner bisnis yang Anda mau satu per satu. Startx menawarkan bisnis-bisnis UKM dari berbagai bidang mulai dari kuliner, jasa, manufaktur dan lain sebagainya yang bisa Anda pilih sesuka hati.</p>
                        <li>Passive Income, tidak perlu terlibat operasional</li>
                        <p>Berinvestasi pada UKM di Startx memungkinkan Anda mendapatkan dividen usaha UKM tanpa perlu terlibat dalam operasional UKM yang Anda investasikan.</p>
                        <li>Praktis, Semua Layanan Dalam Satu Platform</li>
                        <p>Proses memilih UKM, investasi hingga mendapatkan dividen bisa dilakukan di satu platform saja.</p>
                        <li>Modal Terjangkau</li>
                        <p>Dengan sistem Equity Crowdfunding maka berinvestasi dengan memiliki sebagian saham di bisnis UKM bisa dimulai dengan modal yang relatif terjangkau.</p>
                        <li>Anda berhak menentukan bisnis yang akan listing</li>
                        <p>Melalui fitur Pralisting, para pengguna bisa bersama-sama melakukan review dan penilaian terhadap bisnis yang mengajukan menjadi diri menjadi Penerbit di Startx. Sehingga penilaian bisa lebih objektif dan komprehensif. Bandingkan jika Anda berinvestasi sendiri, Anda akan dan harus melakukan penilaian sendiri terhadap UKM tersebut.</p>
                        </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <!--================End Home Banner Area =================-->

        <!--================ Start About Area =================-->
        <section class="about_area section_gap">
            <div class="container">
                <div class="row h_blog_item">




                </div>
            </div>
        </section>
        <!--================ End About Area =================-->

        <!--================ Start Feature Area =================-->
        <section class="feature_area section_gap_top title-bg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="main_title">
                            <h4 class="mb-3 text-white">Awesome Feature</h4>
                            <p>
                                Replenish man have thing gathering lights yielding shall you
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single_feature">

                            <div class="desc">
                                <h4 class="mt-3 mb-2">Ajukan Permodalan</h4>
                                <p>
                                    One make creepeth, man bearing theira firmament won't great
                                    heaven
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="single_feature">

                            <div class="desc">
                                <h4 class="mt-3 mb-2">Berinvestasi</h4>
                                <p>
                                    One make creepeth, man bearing theira firmament won't great
                                    heaven
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="single_feature">

                            <div class="desc">
                                <h4 class="mt-3 mb-2">Hubungi Kami</h4>
                                <p>
                                    One make creepeth, man bearing theira firmament won't great
                                    heaven
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================ End Feature Area =================-->

        <!--================ Start Testimonial Area =================-->
        <div class="testimonial_area section_gap">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="main_title">
                            <h4 class="mb-3">Client say about me</h4>
                            <p>
                                Replenish man have thing gathering lights yielding shall you
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="testi_slider owl-carousel">
                        <div class="testi_item">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <img src="{{asset('edustage/img/testimonials/t1.jpg')}}" alt="" />
                                </div>
                                <div class="col-lg-8">
                                    <div class="testi_text">
                                        <h4>Elite Martin</h4>
                                        <p>
                                            Him, made can't called over won't there on divide there
                                            male fish beast own his day third seed sixth seas unto.
                                            Saw from
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testi_item">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <img src="{{asset('edustage/img/testimonials/t2.jpg')}}" alt="" />
                                </div>
                                <div class="col-lg-8">
                                    <div class="testi_text">
                                        <h4>Davil Saden</h4>
                                        <p>
                                            Him, made can't called over won't there on divide there
                                            male fish beast own his day third seed sixth seas unto.
                                            Saw from
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testi_item">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <img src="{{asset('edustage/img/testimonials/t1.jpg')}}" alt="" />
                                </div>
                                <div class="col-lg-8">
                                    <div class="testi_text">
                                        <h4>Elite Martin</h4>
                                        <p>
                                            Him, made can't called over won't there on divide there
                                            male fish beast own his day third seed sixth seas unto.
                                            Saw from
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testi_item">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <img src="{{asset('edustage/img/testimonials/t2.jpg')}}" alt="" />
                                </div>
                                <div class="col-lg-8">
                                    <div class="testi_text">
                                        <h4>Davil Saden</h4>
                                        <p>
                                            Him, made can't called over won't there on divide there
                                            male fish beast own his day third seed sixth seas unto.
                                            Saw from
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testi_item">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <img src="{{asset('edustage/img/testimonials/t1.jpg')}}" alt="" />
                                </div>
                                <div class="col-lg-8">
                                    <div class="testi_text">
                                        <h4>Elite Martin</h4>
                                        <p>
                                            Him, made can't called over won't there on divide there
                                            male fish beast own his day third seed sixth seas unto.
                                            Saw from
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testi_item">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <img src="{{asset('edustage/img/testimonials/t2.jpg')}}" alt="" />
                                </div>
                                <div class="col-lg-8">
                                    <div class="testi_text">
                                        <h4>Davil Saden</h4>
                                        <p>
                                            Him, made can't called over won't there on divide there
                                            male fish beast own his day third seed sixth seas unto.
                                            Saw from
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!--================ End Testimonial Area =================-->

        @include('template.footer')

</body>

</html>
