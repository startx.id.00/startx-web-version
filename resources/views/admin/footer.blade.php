    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2020 
        <a class="text-bold-800 grey darken-2" href="" target="_blank">Startx.id</a></span><span class="float-md-right d-none d-lg-block">Hand-crafted & Made with<i class="ft-heart pink"></i><span id="scroll-top"></span></span></p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('admin/vendors/js/material-vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('admin/vendors/js/charts/chartist.min.js')}}"></script>
    <script src="{{asset('admin/vendors/js/charts/chartist-plugin-tooltip.min.js')}}"></script>
    <script src="{{asset('admin/vendors/js/charts/raphael-min.js')}}"></script>
    <script src="{{asset('admin/vendors/js/charts/morris.min.js')}}"></script>
    <script src="{{asset('admin/vendors/js/timeline/horizontal-timeline.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('admin/js/core/app-menu.js')}}"></script>
    <script src="{{asset('admin/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('admin/js/scripts/pages/material-app.js')}}"></script>
    <script src="{{asset('admin/js/scripts/pages/dashboard-ecommerce.js')}}"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>