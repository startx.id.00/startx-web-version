<?php

namespace Database\Factories;

use App\Models\Proposal;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProposalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Proposal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->randomDigitNotNull,
        'company_id' => $this->faker->randomDigitNotNull,
        'title' => $this->faker->word,
        'slug' => $this->faker->word,
        'description' => $this->faker->word,
        'summary' => $this->faker->word,
        'price' => $this->faker->randomDigitNotNull,
        'shares' => $this->faker->randomDigitNotNull,
        'period' => $this->faker->randomDigitNotNull,
        'due' => $this->faker->word,
        'code' => $this->faker->word,
        'image' => $this->faker->word,
        'file' => $this->faker->word,
        'status' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
