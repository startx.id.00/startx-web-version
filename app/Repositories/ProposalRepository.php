<?php

namespace App\Repositories;

use App\Models\Proposal;
use App\Repositories\BaseRepository;

/**
 * Class ProposalRepository
 * @package App\Repositories
 * @version November 29, 2020, 2:52 pm UTC
*/

class ProposalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'company_id',
        'title',
        'slug',
        'description',
        'summary',
        'price',
        'shares',
        'period',
        'due',
        'code',
        'image',
        'file',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Proposal::class;
    }
}
