@include('admin.head')

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern material-vertical-layout material-layout 2-columns   fixed-navbar"
    data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">


    @include('admin.header')

    @include('admin.main-menu')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
            <div class="content-wrapper">
                <div class="content-body">

                @if (!Auth::guest())

                @yield('content')

                </div>
            </div>
        </div>
    </div>
    @else

    @endif

    @include('admin.footer')
