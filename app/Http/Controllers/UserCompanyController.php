<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Category;
use Auth;

class UserCompanyController extends Controller
{
    public function index()
    {
        $company = Company::where('user_id',Auth::user()->id)->first();
        $categories = Category::get();
        
        return view('user.company')->with(['company' => $company, 'categories' => $categories ]);
    }

    public function store(Request $request)
    {
        
        $company = Company::where('user_id',Auth::user()->id)->first();
        if($company == null){
            $company = new Company;
        }
        $company->user_id = Auth::user()->id;
        $company->name  = $request->name;
        $company->address   = $request->address;
        $company->nib   = $request->nib;
        $company->npwp  = $request->npwp;
        $company->akta  = $request->akta;
        $company->sk    = $request->sk;
        $company->izin  = $request->izin;
        $company->rekening  = $request->rekening;
        $company->telp  = $request->telp;
        $company->map   = $request->map;
        $company->category_id   = $request->category_id;
        if(isset($request->image)){
            $company->image = $request->image;
        }
        $company->save();
    
        // $company = Company::where('user_id',Auth::user()->id)->first();
        // $categories = Category::get();

        // return view('user.company')->with(['company' => $company, 'categories' => $categories ]);
        return redirect('user-company');
    }
}
